"""
USAGE fire_grep.py pattern file file2... for standard input pass "-"
"""

import sys
import fileinput as fi
import fire
import re

def main(*args):
    pattern, paths = args[0], args[1:]
    
    return search(pattern, paths)


def search(pattern, paths):

    for line in fi.input(paths):

        if re.search(pattern, line):
            line = line.rstrip("\n")
            print(line)

                
if __name__ == "__main__":
    fire.Fire(main)
                

                
                