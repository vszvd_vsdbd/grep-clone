#!/usr/bin/env python3

"""
grep

"""

import sys
import fileinput as fi

pattern, path = sys.argv[1:]

with open(path) as p:

    for line in p:
 
            if pattern in line:
                line = line.rstrip("\n")
                print(line)
                
    
    
    


